- Universidad de San Carlos de Guatemala
- Facultad de Ingeniería
- Escuela de Ingeniería en Ciencias y Sistemas
- Organización de Lenguajes y Compiladores 2
- Primer semestre 2020


- J# es un lenguaje de programación basado en c# y javascript, su mayor característica es el control de  ámbitos, lo que nos permite llevar un mejor manejo del consumo de memoria. Este lenguaje está  formado por un conjunto de herramientas muy flexibles que pueden ampliarse fácilmente mediante  paquetes, librerías o definiendo nuestras propias funciones. Este software también cuenta con un  entorno de desarrollo integrado (IDE) que proporciona servicios integrales para facilitarle al  programador el desarrollo de aplicaciones. 

- El flujo principal de la aplicación comienza con un archivo en lenguaje J# que se compila desde el IDE. El  proceso de compilación retornará un archivo con formato de código de 3 direcciones. Un flujo alterno del programa  consiste en optimizar la salida en C3D previo a ser ejecutada, ya sea optimizándolo con el método de  mirilla o por bloque. 

- El proyecto es una aplicación Web utilizando Javascript. 

- La herramienta para generar todos los analizadores del proyecto es JISON. La documentación  se encuentra en el siguiente enlace http://zaa.ch/jison/docs/ 
