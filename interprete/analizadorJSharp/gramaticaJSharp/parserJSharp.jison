
/* Definición Léxica */
%lex
%options case-insensitive
%%

\s+											// se ignoran espacios en blanco
"//".*										// comentario simple línea
[/][*][^*]*[*]+([^/*][^*]*[*]+)*[/]			// comentario multiple líneas

//"string"			return "RSTRING";
"integer"			return "RINTEGER";
"double"			return "RDOUBLE";
"char"			    return "RCHAR";
"boolean"			return "RBOOLEAN";

"import"   return "RIMPORT";
"true"   return "RTRUE";
"switch"   return "RSWITCH";
"continue"   return "RCONTINUE";
"private"   return "RPRIVATE";
"define"   return "RDEFINE";
"try"   return "RTRY";
"var"   return "RVAR";
"false"   return "RFALSE";
"case"   return "RCASE";
"return"   return "RRETURN";
"void"   return "RVOID";
"as"   return "RAS";
"catch"   return "RCATCH";
"const"   return "RCONST";
"if"   return "RIF";
"default"   return "RDEFAULT";
"print"   return "RPRINT";
"for"   return "RFOR";
"strc"   return "RSTRC";
"throw"   return "RTHROW";
"global"   return "RGLOBAL";
"else"   return "RELSE";
"break"   return "RBREAK";
"public"   return "RPUBLIC";
"while"   return "RWHILE";
"do"  return "RDO";


","					return "COMA";
";"					return "PTCOMA";
"("					return "PARIZQ";
")"					return "PARDER";
"{"					return "LLAVIZQ";
"}"					return "LLAVDER";
"["					return "CORCHETEIZQ";
"]"					return "CORCHETEDER";

"+"					return "MAS";
"-"					return "MENOS";
"*"					return "POR";
"/"					return "DIVIDIDO";
"%"					return "PORCENTAJE";
"^^"				return "POTENCIA";

"&&"				return "AND"
"||"				return "OR";
"^"					return "XOR";

"<="				return "MENOR_IGUAL";
">="				return "MAYOR_IGUAL";
"=="				return "IGUALX2";
"==="				return "IGUALX3";
"!="				return "DIFERENTE_QUE";

"<"					return "MENOR_QUE";
">"					return "MAYOR_QUE";
"="					return "IGUAL";
":="				return "DOSPUNTOS_IGUAL";


"!"					return "NOT";

\"[^\"]*\"				return "CADENA";
[0-9]+("."[0-9]+)\b  	return "DECIMAL";
[0-9]+\b				return "ENTERO";
[a-zA-Z][a-zA-Z0-9_]*	return "IDENTIFICADOR";
\'([^\'']+|[\r\n])*\'   return "CHAR"


<<EOF>>				return "EOF";
.					{ console.error("Este es un error léxico, token: \"" + yytext + "\", en la linea: " + yylloc.first_line + ", en la columna: " + yylloc.first_column); }

/lex



/* Asociación de operadores y precedencia */

%left "XOR"
%left "OR"
%left "AND"
%left "IGUALX2" "DIFERENTE_QUE" "IGUALX3"
%left "MENOR_QUE" "MAYOR_QUE" "MENOR_IGUAL" "MAYOR_IGUAL"
%left "MAS" "MENOS"
%left "POR" "DIVIDIDO" "PORCENTAJE"
%right "POTENCIA"
%right UMENOS, "NOT"

%start ini

%% /* Definición de la gramática */

ini : instrucciones EOF  { return new ArbolST($1); }
;

instrucciones :
    instrucciones instruccion { $$ = $1; $$.push($2); }
	| instruccion { $$ = [$1]; }
;

instruccion:
  declaracion_var PTCOMA { $$ = $1;}
 | funcion
 | asignacion_var PTCOMA { $$ = $1;}
 | imprimir PTCOMA { $$ = $1;}
 | llamada_funcion PTCOMA{ $$ = $1;}
 //| error  { console.error("Este es un error sintáctico, token: \"" + yytext + "\", en la linea: " + this._$.first_line + ", en la columna: " + this._$.first_column); }
;


funcion:
      RVOID IDENTIFICADOR PARIZQ  PARDER  LLAVIZQ sentencias LLAVDER                    { $$ = new Funcion(["void"], $2, [], $6, @1.first_line, @1.first_column); }
    | RVOID IDENTIFICADOR PARIZQ  parametros PARDER  LLAVIZQ sentencias LLAVDER         { $$ = new Funcion(["void"], $2, $4, $7, @1.first_line, @1.first_column); }
    | tipo_var IDENTIFICADOR PARIZQ  PARDER  LLAVIZQ sentencias LLAVDER                 { $$ = new Funcion([$1], $2, [], $6, @1.first_line, @1.first_column); }
    | tipo_var IDENTIFICADOR PARIZQ  parametros PARDER  LLAVIZQ sentencias LLAVDER      { $$ = new Funcion([$1], $2, $4, $7, @1.first_line, @1.first_column); }
    | tipo_var CORCHETEIZQ  CORCHETEDER IDENTIFICADOR PARIZQ  PARDER  LLAVIZQ sentencias LLAVDER { $$ = new Funcion(["array", $1], $3, [], $7, @1.first_line, @1.first_column); }
    | tipo_var CORCHETEIZQ  CORCHETEDER IDENTIFICADOR PARIZQ  parametros PARDER  LLAVIZQ sentencias LLAVDER { $$ = new FuncionAlto(["array", $1], $3, $5, $8, @1.first_line, @1.first_column); }   
;

parametros:
    parametros ',' parametro        { $$ = $1; $$.push($3); }
    | parametro                     { $$ = [$1]; }
;

parametro:
      tipo_var IDENTIFICADOR                           { $$ = [[$1], $2.toLocaleLowerCase()]; }
    | tipo_var CORCHETEIZQ  CORCHETEDER IDENTIFICADOR  { $$ = [["array",$1], $3.toLocaleLowerCase()]; }
;

 sentencias: sentencias sentencia { $$ = $1; $$.push($2)}
 | sentencia { $$ = [$1]; }
 ;

sentencia :
   declaracion_var PTCOMA { $$ = $1;}
 | asignacion_var PTCOMA { $$ = $1;}
 | imprimir PTCOMA { $$ = $1;}
 | llamada_funcion PTCOMA{ $$ = $1;}
 ;
 
asignacion_var :
    IDENTIFICADOR IGUAL expresion { $$ = new Asignacion($1, $3, @1.first_line, @1.first_column); }
;

declaracion_var :
    tipo_var id_lista  { $$ = new Declaracion([$1], $2, null, @1.first_line, @1.first_column); }
   |tipo_var id_lista IGUAL expresion {$$ = new Declaracion([$1], $2, $4, @1.first_line, @1.first_column); }
   |type_var IDENTIFICADOR DOSPUNTOS_IGUAL expresion { $$ = new Declaracion2($1, $2, $4, @1.first_line, @1.first_column);}
   ;

llamada_funcion:
    IDENTIFICADOR PARIZQ expr_lista PARDER      { $$ = new LLamadaFun($1, $3, @1.first_line, @1.first_column); }
    | IDENTIFICADOR PARIZQ param_nom PARDER     { $$ = new LlamadaFun($1, $3, @1.first_line, @1.first_column); }
    | IDENTIFICADOR PARIZQ PARDER               { $$ = new LLamadaFun($1, [], @1.first_line, @1.first_column); }
;

expr_lista:
    expr_lista ',' expresion       { $$ = $1; $$.push($3); }
    | expresion                    { $$ = [$1]; }
;

param_nom:
    param_nom ',' id    { $$ = $1; $$.push($3); }
    | id                { $$ = [$1]; }
;   

id_lista :
     id_lista COMA IDENTIFICADOR { $$ = $1, $$.push($3.toLocaleLowerCase()); }
   | IDENTIFICADOR { $$ = [$1.toLocaleLowerCase()]; }
   ;

tipo_var:
     RINTEGER { $$ = "int";}
   | RDOUBLE { $$ = "double";}
   | RCHAR { $$ = "char";}
   | RBOOLEAN { $$ = "boolean";}
   | IDENTIFICADOR { $$ = $1;}
   
   ;

type_var:
     RVAR { $$ = "var";}
   | RCONST { $$ = "const";}
   | RGLOBAL { $$ = "global";}
   ;

 imprimir:
     RPRINT PARIZQ expresion PARDER   { $$ = new Imprimir($3, @1.first_line, @1.first_column); }
     ;

  expresion_logica :
    expresion AND expresion  { $$ = new Logica($1,$3,"and",@1.first_line, @1.first_column);}
  | expresion OR expresion   { $$ = new Logica($1,$3,"or",@1.first_line, @1.first_column);}
	| expresion XOR expresion  { $$ = new Logica($1,$3,"xor",@1.first_line, @1.first_column);}
	| NOT expresion	 { $$ = new Logica($2,null,"not",@1.first_line, @1.first_column);}
;

expresion_relacional :
    expresion MAYOR_QUE expresion{ $$ = new Relacional($1,$3,"mayor_que",@1.first_line, @1.first_column);}
	| expresion MENOR_QUE expresion{ $$ = new Relacional($1,$3,"menor_que",@1.first_line, @1.first_column);}
	| expresion MAYOR_IGUAL expresion{ $$ = new Relacional($1,$3,"mayor_igual",@1.first_line, @1.first_column);}
	| expresion MENOR_IGUAL expresion{ $$ = new Relacional($1,$3,"menor_igual",@1.first_line, @1.first_column);}
	| expresion DIFERENTE_QUE expresion{ $$ = new Relacional($1,$3,"diferente_que",@1.first_line, @1.first_column);}
	| expresion IGUALX2 expresion{ $$ = new Relacional($1,$3,"igualx2",@1.first_line, @1.first_column);}
	| expresion IGUALX3 expresion{ $$ = new Relacional($1,$3,"igualx3",@1.first_line, @1.first_column);}
;

expresion_aritmetica:
    MENOS expresion %prec UMENOS { $$ = new Aritmetica($2,null,"unario",@1.first_line, @1.first_column);}
	| expresion MAS expresion	{ $$ = new Aritmetica($1,$3,"suma",@1.first_line, @1.first_column);}
	| expresion MENOS expresion { $$ = new Aritmetica($1,$3,"resta",@1.first_line, @1.first_column);}
	| expresion POR expresion	{ $$ = new Aritmetica($1,$3,"multiplicacion",@1.first_line, @1.first_column);}
	| expresion DIVIDIDO expresion { $$ = new Aritmetica($1,$3,"division",@1.first_line, @1.first_column);}
	| expresion PORCENTAJE expresion { $$ = new Aritmetica($1,$3,"modulo",@1.first_line, @1.first_column);}
	| expresion POTENCIA expresion { $$ = new Aritmetica($1,$3,"potencia",@1.first_line, @1.first_column);}
;

expresion:
     CADENA { $$ = new Primitivo("string", $1, @1.first_line, @1.first_column);}
    |ENTERO { $$ = new Primitivo("int", Number($1), @1.first_line, @1.first_column)}
    |DECIMAL { $$ = new Primitivo("double", Number($1), @1.first_line, @1.first_column)}
    |CHAR { $$ = new Primitivo("char", $1, @1.first_line, @1.first_column)}
    |RTRUE { $$ = new Primitivo("boolean", true, @1.first_line, @1.first_column)}
    |RFALSE { $$ = new Primitivo("boolean",false, @1.first_line, @1.first_column)}
    |IDENTIFICADOR { $$ = new Identificador($1.toLocaleLowerCase(), @1.first_line, @1.first_column);}
    |RNULL { $$ = new Primitivo("null", null, @1.first_line, @1.first_column);}
    |PARIZQ expresion PARDER { $$ = $2; }
    |expresion_aritmetica { $$ = $1;}
    |expresion_logica { $$ = $1;}
    |expresion_relacional { $$ = $1;}
    |llamada_funcion  { $$ = $1;}
    ;


