//var fs = require('fs'); 

function ejecutarParserJSharp() {

    var entrada = editorTxt.getValue();
    //alert(entrada);
    try {
        // invocamos a nuestro parser con el contendio del archivo de entradas
        var result = parserJSharp.parse(entrada);
        salidaTxt.setValue("Analizador Finalizado...");
        //salidaTxt.setValue(JSON.stringify(result, null, 2));
        //imprimimos en un archivo el contendio del AST en formato JSON
        //fs.writeFileSync('./ast.json', JSON.stringify(ast, null, 2));

        let ent = new Entorno(null);
        let ent3D = new Entorno3D();
        for (let i = 0; i < result.instrucciones.length; i++) {
            result.instrucciones[i].analizar(ent3D, ent);
        }

        if (Singleton.errores.length == 0) {
            let traductor = new Traducir();
            codigo3D = traductor.generar3D(result, ent3D, ent);
            codido3DTxt.setValue(codigo3D);

        } else {
            for (let i = 0; i < Singleton.errores.length; i++) {
                let error = Singleton.errores[i];
                salidaTxt.setValue("ERROR: " + error.tipo + ", " + error.descripcion + ", línea: " + error.fila + ", columna: " + error.columna);
            }
        }

    } catch (e) {
        salidaTxt.setValue("Entró a catch--> " + String(e));
        return;
    }
}