class Traducir {


    constructor() {

    }

    generar3D(ast, ent3D, ent) {
        
        let codigo = "";
        let salida = "";
        let etq = ent3D.getL();

        for (let i = 0; i < ast.instrucciones.length; i++) {
            salida += ast.instrucciones[i].traducir(ent3D, ent);
        }


        let declaraciones = "var ";
        console.log(this.ent3D)
        for (let i = 0; i < ent3D.contadorTmp; i++) {
            if (i != 0) {
                declaraciones += ", ";
            }
            declaraciones += "t" + i;
        }
        declaraciones += ";\n";

        declaraciones += "var p, h;\n";
        declaraciones += "var Heap[];\n";
        declaraciones += "var Stack[];\n";
        for (let x = 0; x < ent3D.heap; x++) {
            declaraciones += "Heap[" + x + "] = 0;\n";
        }
        declaraciones += "h = " + ent3D.heap + ";\n";
        declaraciones += "goto " + etq + ";\n";

        codigo = declaraciones;

        codigo += etq + ":\n";

        codigo += salida;

        //   codigo += codigoGlobales;
        /*   if (ent3D.existeFuncion("principal")) {
               codigo += "call principal;"
           }*/

        return codigo;
    }

}