
class Identificador {

    constructor(id, fila, columna) {
        this.id = id.toLocaleLowerCase();
        this.fila = fila;
        this.columna = columna;
    }

    analizar(ent3D, ent) {
        if (!ent.existe(this.id)) {
            var error = new ErrorAst("Semántico", "No se encontró id: " + this.id, this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
        let simbolo = ent.getSimbolo(this.id);
        return simbolo.tipo;
    }

    getTipo(ent) {
        if (!ent.existe(this.id)) {
            var error = new ErrorAst("Semántico", "No se encontró id: " + this.id, this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
        let simbolo = ent.getSimbolo(this.id);
        return simbolo.tipo;
    }


    traducir(ent3D, ent) {
        //  let codigo = "#BEGIN ID, fila " + this.fila + " columna " + this.columna + "\n";
        let codigo = "";
        let sim = ent.getSimbolo(this.id);
        if (sim.ent == "local") {
            let temp = ent3D.getT();
            codigo += temp + " = p + " + sim.puntero + ";\n";
            let tempR = ent3D.getT();
            codigo += tempR + " = Stack[" + temp + "];\n";
            ent3D.addTempNoUtil(tempR);
        } else {
            let temp = ent3D.getT();
            codigo += temp + " = " + sim.puntero + ";\n";
            let tempR = ent3D.getT();
            codigo += tempR + " = Heap[" + temp + "];\n";
            ent3D.addTempNoUtil(tempR);
        }

        //codigo += "#END ID\n"
        return codigo
    }
}

