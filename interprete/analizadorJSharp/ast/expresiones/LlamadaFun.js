class LLamadaFun {
    constructor(id, parametros, fila, columna) {
        this.id = id.toLocaleLowerCase();
        this.parametros = parametros;
        this.fila = fila;
        this.columna = columna;
        this.id2 = "";
    }
    analizar(ent3D, ent) {
        let id = this.id;
        let tipo_parametros = [];
        for (let i = 0; i < this.parametros.length; i++) {
            let tipo = this.parametros[i].analizar(ent);
            if (tipo instanceof ErrorAst) {
                return tipo;
            }
            if (tipo[0] == "array") {
                id += "_arreglo_" + tipo[1];
            } else {
                id += "_" + tipo[0];
            }
            tipo_parametros.push(tipo);
        }
        if (!ent3D.existeFun(id)) {
            let funciones = ent3D.getListaFun(this.id);
            let encontro = false;
            let tipo = [];
            for (let i = 0; i < funciones.length; i++) {
                encontro = this.verificar(ent3D, funciones[i], tipo_parametros);
                if (encontro) {
                    this.id2 = funciones[i].id;
                    tipo = funciones[i].tipo;
                    break;
                }
            }
            if (!encontro) {
                let error = new ErrorAst("Semántico", "Llamada a función no válida", this.fila, this.columna);
                (error);
                return error;Singleton.addError
            }
            return tipo;
        }
        this.id2 = id;
        let fun = ent3D.getF(id);
        return fun.tipo;
    }
    verificar(ent3D, funcion, tipo_valores) {

        if (funcion.parametros.length != tipo_valores.length) {
            return false;
        }
        for (let i = 0; i < funcion.parametros.length; i++) {
            let tipo = funcion.parametros[i][0];
            let tipoExpr = tipo_valores[i];
            if (tipo[0] == tipoExpr[0]) {
                if (tipo[0] == "array" && tipoExpr[0] == "array") {
                    if (tipo[1] != tipoExpr[1]) {
                        return false;
                    }
                }
                continue;
            }
            if (!((tipo[0] == "int" && tipoExpr[0] == "char")
                || (tipo[0] == "double" && tipoExpr[0] == "int")
                || (tipo[0] == "double" && tipoExpr[0] == "char")
                || (tipo[0] == "string" && tipoExpr[0] == "null")
                || (tipo[0] == "array" && tipoExpr[0] == "null"))
                || (ent3D.existeEstr(tipo[0]) && tipoExpr[0] == "null")) {
                return false;
            }
        }
        return true;
    }
    traducir(ent3D, ent) {
        let codigo = "#BENGIN Llamada a: " + this.id + ", fila " + this.fila + " col " + this.columna + "\n";

     
        let val_parametros = [];
        for (let i = 0; i < this.parametros.length; i++) {
            codigo += this.parametros[i].traducir(ent3D, ent);
            let tem = ent3D.getTempActual();
            val_parametros.push(tem);
        }
   

        for (let i = 0; i < val_parametros.length; i++) {
            ent3D.suprTempNoUtil(val_parametros[i]);
        }
     


        codigo += "p = p + " + ent3D.stack + ";\n";
     

        let temp2 = ent3D.getT();
        for (let i = 0; i < ent3D.tempsNoUtilizados.length; i++) {
            codigo += temp2 + " = p + " + i + ";\n";
            codigo += "Stack[" + temp2 + "] = " + ent3D.tempsNoUtilizados[i] + ";\n";
        }
        codigo += "p = p + " + ent3D.tempsNoUtilizados.length + ";\n";
       

        
        let temp3 = ent3D.getT();
        codigo += temp3 + " = p + 0;\n"
        codigo += "Stack[" + temp3 + "] = 0;\n"
        for (let i = 0; i < this.parametros.length; i++) {
            codigo += temp3 + " = p + " + (i + 1) + ";\n"
            codigo += "Stack[" + temp3 + "] = " + val_parametros[i] + ";\n"
        }

        codigo += "call " + this.id2 + ";\n"
       

        let tempR = ent3D.getT();
        codigo += tempR + " = p + 0;\n";
        let tempR2 = ent3D.getT();
        codigo += tempR2 + " = Stack[" + tempR + "];\n";
        

        codigo += "p = p - " + ent3D.tempsNoUtilizados.length + ";\n";
        let tempRec = ent3D.getT();
        for (let i = 0; i < ent3D.tempsNoUtilizados.length; i++) {
            codigo += tempRec + " = p + " + i + ";\n";
            codigo += ent3D.tempsNoUtilizados[i] + " = Stack[" + tempRec + "];\n";
        }
        codigo += "p = p - " + ent3D.stack + ";\n";


        let tempValorReturn = ent3D.getT();

        codigo += tempValorReturn + " = " + tempR2 + ";\n";
        ent3D.addTempNoUtil(tempValorReturn);
        codigo += "#END llamada\n"
        return codigo;
    }

}