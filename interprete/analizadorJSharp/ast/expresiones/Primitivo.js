class Primitivo {

    constructor(tipo, valor, fila, columna) {
        this.tipo = tipo;
        this.valor = valor;
        this.fila = fila;
        this.columna = columna;
    }

    analizar(ent3D, ent) {
        return [this.tipo];
    }

    getTipo(ent3D, ent) {
        return [this.tipo];
    }

    traducir(ent3D, ent) {
        let codigo = "";
        let temp;

        switch (this.tipo) {
            case "string": 
                let temp1 = ent3D.getT();
                codigo += temp1 + " = h;\n";
                codigo += "Heap[h] = " + this.valor.length + ";\n";
                codigo += "h = h + 1;\n";
                for (let i = 0; i < this.valor.length; i++) {
                    codigo += "Heap[h] = " + this.valor[i].charCodeAt(0) + ";\n";
                    codigo += "h = h + 1;\n"; 
                }
                let temp2 = ent3D.getT();
                codigo += temp2 + " = " + temp1 + ";\n";
                ent3D.addTempNoUtil(temp1);
                break;
            case "char":
                temp = ent3D.getT();
                codigo = temp + " = " + this.valor.charCodeAt(0) + ";\n";
                ent3D.addTempNoUtil(temp);
                break;
            case "int":
                temp = ent3D.getT();
                codigo = temp + " = " + this.valor + ";\n";
                ent3D.addTempNoUtil(temp);
                break;
            case "double":
                temp = ent3D.getT();
                codigo = temp + " = " + this.valor + ";\n";
                ent3D.addTempNoUtil(temp);
                break;
            case "boolean":
                temp = ent3D.getT();
                if (this.valor) {
                    codigo = temp + " = 1;\n";
                } else {
                    codigo = temp + " = 0;\n";
                }
                ent3D.addTempNoUtil(temp);
                break;
            case "null":
                temp = ent3D.getT();
                codigo = temp + " = -1;\n";
                ent3D.addTempNoUtil(temp);
                break;
            default:
                break;
        }

        return codigo;
    }
}

