class Entorno {
    constructor(padre) {
        this.padre = padre;
        this.tabla = new Map();
    }

    getSimbolo(id) {
        id = id.toLocaleLowerCase();
        for (let e = this; e != null; e = e.padre) {
            let encontrado = (e.tabla.get(id));
            if (encontrado != null) {
                return encontrado;
            }
        }
        return null;
    }


    existeEnActual(id) {
        id = id.toLocaleLowerCase();
        if (e.tabla.has(id)) {
            return true;
        }

        return false;
        //let encontrado = (tabla.get(id));
        ///return encontrado != null;
    }

    existe(id) {
        id = id.toLocaleLowerCase();
        for (let e = this; e != null; e = e.padre) {
            if (e.tabla.has(id)) {
                return true;
            }
        }
        return false;
    }

    agregar(id, simbolo) {
        id = id.toLocaleLowerCase();
        this.tabla.set(id, simbolo);
    }


    existeGlobal(id) {
        id = id.toLocaleLowerCase();
        let e = this.getEntornoGlobal();
        if (e.tabla.has(id)) {
            return true;
        }
        return false;
    }

    
    agregarGlobal(id, simbolo) {
        id = id.toLocaleLowerCase();
        let e = this.getEntornoGlobal();
        e.tabla.set(id, simbolo);
    }

    getEntornoGlobal() {
        for (let e = this; e != null; e = e.padre) {
            if (e.padre == null) {
                return e;
            }
        }
        return this;
    }
}
