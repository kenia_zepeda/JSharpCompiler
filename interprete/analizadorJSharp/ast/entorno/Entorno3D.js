class Entorno3D {

    constructor() {
        this.contadorEtq = 1;
        this.contadorTmp = 1;
        this.salidas = [];
        this.inicios = [];
        this.ambitos = [];
        this.temporales = [];
        this.tempsNoUtilizados = [];
        this.nombres = [];
        this.stack = 0;
        this.heap = 0;

        this.funcionSizeActual;
        this.displayBreak = [];
        this.displayContinue = [];
        this.entorno = "global";
        this.ambito = "global";
        this.funciones = [];
        this.tipoFuncion = ["void"];
        this.finFuncion = "";
        this.estructuras = [];
        this.codigoEstructuras = [];
        this.displayTry = [];
        this.simbolos = [];
        this.globales = [];
        this.advertencias = [];
        this.tipos = ["int", "double", "char", "boolean", "string"];

    }

    getT() {
        let temp = "t" + this.contadorTmp;
        this.contadorTmp++;
        return temp;
    }

    getTempActual() {
        return "t" + (this.contadorTmp - 1);
    }

    getL() {
        let etiq = "L" + this.contadorEtq;
        this.contadorEtq++;
        return etiq;
    }

    addTempNoUtil(temp) {
        this.tempsNoUtilizados.push(temp);
    }

    suprTempNoUtil(temp) {
        this.tempsNoUtilizados.splice(this.tempsNoUtilizados.indexOf(temp), 1);
    }

    agregarGlobal(nuevo) {
        this.globales.push(nuevo);
    }


    getHeap() {
        return this.heap++;
    }

    getStack() {
        return this.stack++;
    }

    getEstr(id) {
        for (let x = 0; x < this.estructuras.length; x++) {
            if (id == this.estructuras[x].id) {
                return this.estructuras[x];
            }
        }
        return false;
    }
    addEstr(est) {
        this.estructuras.push(est);
    }
    addEstrInstrucciones(cod) {
        this.codigoEstructuras.push(cod);
    }

    existeEstr(tipo) {
        for (let x = 0; x < this.estructuras.length; x++) {
            if (tipo == this.estructuras[x].id) {
                return true;
            }
        }
        return false;
    }

    existeFun(id) {
        for (let x = 0; x < this.funciones.length; x++) {
            if (id == this.funciones[x].id) {
                return true;
            }
        }
        return false;
    }
    addFuncion(nueva) {
        this.funciones.push(nueva);
    }
    getF(id) {
        for (let x = 0; x < this.funciones.length; x++) {
            if (id == this.funciones[x].id) {
                return this.funciones[x];
            }
        }
        return false;
    }
    getListaFun(id) {
        let funciones = [];
        for (let x = 0; x < this.funciones.length; x++) {
            if (id == this.funciones[x].nombreReal) {
                funciones.push(this.funciones[x]);
            }
        }
        return funciones;
    }
    existeTipo(tipo) {
        tipo = tipo.toLocaleLowerCase();
        for (let x = 0; x < this.tipos.length; x++) {
            if (tipo == this.tipos[x]) {
                return true;
            }
        }
        return false;
    }
    addTipo(tipo) {
        this.tipos.push(tipo);
    }



}