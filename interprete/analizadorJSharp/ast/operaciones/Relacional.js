class Relacional {

    constructor(op1, op2, op, fila, columna) {
        this.op1 = op1;
        this.op2 = op2;
        this.op = op;
        this.fila = fila;
        this.columna = columna;
    }

    analizar(ent3D, ent) {
       //  alert("Operacion Relacional entro a analizar")

        let tipo;
        if (this.op1 != null && this.op2 != null) {
            switch (this.op) {
                case "mayor_que":
                    return this.getTipoRelacional(ent3D, ent);
                case "menor_que":
                    return this.getTipoRelacional(ent3D, ent);
                case "mayor_igual":
                    return this.getTipoRelacional(ent3D, ent);
                case "menor_igual":
                    return this.getTipoRelacional(ent3D, ent);
                case "diferente_que":
                    return this.getTipoIgualdad(ent3D, ent);
                case "igualx2":
                    return this.getTipoIgualdad(ent3D, ent);
                case "igualx3":
                    return this.getTipoIgualdadReferencias(ent3D, ent);
                default:
                    let error = new ErrorAst("Semántico", "Tipo desconocido en operación relacional", this.fila, this.columna);
                    Singleton.addError(error);
                    return error;
                }
        }

        return [tipo];
    }


    traducir(ent3D, ent) {
        if (this.op1 != null && this.op2 != null) {
            switch (this.op) {
                case "mayor_que":
                    return this.traducirRelacional(ent3D, ent, ">");
                case "menor_que":
                    return this.traducirRelacional(ent3D, ent, "<");
                case "mayor_igual":
                    return this.traducirRelacional(ent3D, ent, ">=");
                case "menor_igual":
                    return this.traducirRelacional(ent3D, ent, "<=");
                case "diferente_que":
                    return this.traducirDiferente(ent3D, ent);
                case "igualx2":
                    return this.traducirigualx2(ent3D, ent);
                case "igualx3":
                    return this.traducirIgualx3(ent3D, ent);
                default:
                    let error = new ErrorAst("Semántico", "Tipo desconocido en operación relacional", this.fila, this.columna);
                    Singleton.addError(error);
                    return error;
                }
        }
    }

    getTipoRelacional(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];

        if (op1Tipo == "int") {
            if (op2Tipo == "int" || op2Tipo == "double" || op2Tipo == "char") {
                return ["boolean"]
            }
        } else if (op1Tipo == "double") {
            if (op2Tipo == "int" || op2Tipo == "double" || op2Tipo == "char") {
                return ["boolean"]
            }
        } else if (op1Tipo == "char") {
            if (op2Tipo == "int" || op2Tipo == "double" || op2Tipo == "char") {
                return ["boolean"]
            }
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar operación relacional", this.fila, this.columna);
        }
    }

    getTipoIgualdad(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];

        if (op1Tipo == "int") {
            if (op2Tipo == "int" || op2Tipo == "double" || op2Tipo == "char" || op2Tipo == "boolean") {
                return ["boolean"]
            }
        } else if (op1Tipo == "double") {
            if (op2Tipo == "int" || op2Tipo == "double" || op2Tipo == "char" || op2Tipo == "boolean") {
                return ["boolean"]
            }
        } else if (op1Tipo == "char") {
            if (op2Tipo == "int" || op2Tipo == "double" || op2Tipo == "char" || op2Tipo == "boolean") {
                return ["boolean"]
            }
        } else if (op1Tipo == "boolean") {
            if (op2Tipo == "int" || op2Tipo == "double" || op2Tipo == "char" || op2Tipo == "boolean") {
                return ["boolean"]
            }
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar operación relacional", this.fila, this.columna);
            Singleton.addError(error);
            return error;
        }

    }

    getTipoIgualdadReferencias(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];

        if (op1Tipo == "string" && op2Tipo == "string") {
            return ["boolean"]
        } else if (op1Tipo == "arreglo" && op2Tipo == "arreglo") {
            return ["boolean"]
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar operación relacional", this.fila, this.columna);
            Singleton.addError(error);
            return error;
        }
    }

    traducirRelacional(ent3D, ent, op) {
        let codigo = "#BEGIN \"Exp Relacional\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let op1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op1);

        codigo += this.op2.traducir(ent3D, ent);
        let op2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op2);

        let tempR = ent3D.getT();
        let Lv = ent3D.getL();
        let Lf = ent3D.getL();
        codigo += "if (" + op1 + op + op2 + ") goto " + Lv + ";\n";
        codigo += tempR + " = 0;\n";
        codigo += "goto " + Lf + ";\n";
        codigo += Lv + ":\n"
        codigo += tempR + " = 1;\n";
        codigo += Lf + ":\n"
        ent3D.suprTempNoUtil(op1);
        ent3D.suprTempNoUtil(op2);
        ent3D.addTempNoUtil(tempR);
        codigo += "#END \"Exp Relacional\"\n"
        return codigo;
    }

    traducirDiferente(ent3D, ent) {
        let codigo = "#BEGIN \"Exp Relacional\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let op1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op1);
        let tipo1 = this.op1.analizar(ent3D, ent);
        codigo += this.op2.traducir(ent3D, ent);
        let op2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op2);
        let tipo2 = this.op2.analizar(ent3D, ent);
        if (tipo1[0] == "string" && tipo2[0] == "string") {

        } else {
            let tempR = ent3D.getT();
            let Lv = ent3D.getL();
            let Lf = ent3D.getL();
            codigo += "if (" + op1 + " <> " + op2 + ") goto " + Lv + ";\n";
            codigo += tempR + " = 0;\n";
            codigo += "goto " + Lf + ";\n";
            codigo += Lv + ":\n"
            codigo += tempR + " = 1;\n";
            codigo += Lf + ":\n"
            ent3D.addTempNoUtil(tempR);
        }
        ent3D.suprTempNoUtil(op1);
        ent3D.suprTempNoUtil(op2);
        codigo += "#END \"Exp Relacional\"\n"
        return codigo;
    }

    traducirigualx2(ent3D, ent) {
        let codigo = "#BEGIN \"Exp Relacional\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let op1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op1);
        let tipo1 = this.op1.analizar(ent3D, ent);
        codigo += this.op2.traducir(ent3D, ent);
        let op2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op2);
        let tipo2 = this.op2.analizar(ent3D, ent);
        if (tipo1[0] == "string" && tipo2[0] == "string") {
        
        } else {
            let tempR = ent3D.getT();
            let Lv = ent3D.getL();
            let Lf = ent3D.getL();
            codigo += "if (" + op1 + " == " + op2 + ") goto " + Lv + ";\n";
            codigo += tempR + " = 0;\n";
            codigo += "goto " + Lf + ";\n";
            codigo += Lv + ":\n"
            codigo += tempR + " = 1;\n";
            codigo += Lf + ":\n"

            ent3D.addTempNoUtil(tempR);
        }
        ent3D.suprTempNoUtil(op1);
        ent3D.suprTempNoUtil(op2);
        codigo += "#END \"Exp Relacional\"\n"
        return codigo;
    }

    traducirIgualx3(ent3D, ent) {
        let codigo = "#BEGIN \"Exp Relacional\", fila: " + this.fila + " col: " + this.columna + "\n";
        codigo += this.op1.traducir(ent3D, ent);
        let op1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op1);
        let tipo1 = this.op1.analizar(ent3D, ent);
        codigo += this.op2.traducir(ent3D, ent);
        let op2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(op2);
        let tipo2 = this.op2.analizar(ent3D, ent);

        let tempR = ent3D.getT();
        let Lv = ent3D.getL();
        let Lf = ent3D.getL();
        codigo += "if (" + op1 + " == " + op2 + ") goto " + Lv + ";\n";
        codigo += tempR + " = 0;\n";
        codigo += "goto " + Lf + ";\n";
        codigo += Lv + ":\n"
        codigo += tempR + " = 1;\n";
        codigo += Lf + ":\n"

        ent3D.addTempNoUtil(tempR);

        ent3D.suprTempNoUtil(op1);
        ent3D.suprTempNoUtil(op2);
        codigo += "#END \"Exp Relacional\"\n"
        return codigo;
    }

}