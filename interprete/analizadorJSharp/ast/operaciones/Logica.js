class Logica {

    constructor(op1, op2, op, fila, columna) {
        this.op1 = op1;
        this.op2 = op2;
        this.op = op;
        this.fila = fila;
        this.columna = columna;
    }

    analizar(ent3D, ent) {
        let tipo;

        if (this.op1 != null && this.op2 != null) {
            switch (this.op) {
                case "and":
                    return this.getTipo(ent3D, ent);
                case "or":
                    return this.getTipo(ent3D, ent);
                case "xor":
                    return this.getTipo(ent3D, ent);
                case "not":
                    return this.getTipoNot(ent3D, ent);
                default:
                    let error = new ErrorAst("Semántico", "Tipo desconocido en operación lógica", this.fila, this.columna);
            }
        }
        return [tipo];
    }

    traducir(ent3D, ent) {
        if (this.op1 != null && this.op2 != null) {
            switch (this.op) {
                case "and":
                    return this.traducirAnd(ent3D, ent);
                case "or":
                    return this.traducirOr(ent3D, ent);
                case "xor":
                    return this.traducirXor(ent3D, ent);
                case "not":
                    return this.traducirNot(ent3D, ent);
                default:
                    let error = new ErrorAst("Semántico", "Tipo desconocido en operación lógica", this.fila, this.columna);
                    Singleton.addError(error);
                    return error;
                }
        }
    }

    getTipo(ent3D, ent) {

        let op1Tipo = this.op1.analizar(ent3D, ent);
        let op2Tipo = this.op2.analizar(ent3D, ent);

        if (op1Tipo[0] == "boolean" && op2Tipo[0] == "boolean") {
            return ["boolean"];
        } else {
            let error = new ErrorAst("Tipo de dato incorrecto, se espera un booleano", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }

    getTipoNot(ent3D, ent) {

        let op1Tipo = this.op1.analizar(ent3D, ent);

        if (op1Tipo[0] == "boolean") {
            return ["boolean"];
        } else {
            let error = new ErrorAst("Tipo de dato incorrecto, se espera un booleano", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }

    traducirAnd(ent3D, ent) {
        let codigo = "#BEGIN \"Expresión Lógica\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let tOp1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp1);

        let Lv = ent3D.getL();
        let Lf1 = ent3D.getL();
        let temp = ent3D.getT();
        codigo += temp + " = 0;\n";
        ent3D.addTempNoUtil(temp);
        codigo += "if (" + tOp1 + " == 1) goto " + Lv + ";\n";
        codigo += "goto " + Lf1 + ";\n";
        ent3D.suprTempNoUtil(tOp1);
        codigo += Lv + ":\n";

        codigo += this.op2.traducir(ent3D, ent);
        let tOp2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp2);
        let Lv2 = ent3D.getL();
        let Lf2 = ent3D.getL();
        codigo += "if (" + tOp2 + " == 1) goto " + Lv2 + ";\n";
        codigo += "goto " + Lf2 + ";\n";
        ent3D.suprTempNoUtil(tOp2);

        codigo += Lv2 + ":\n";
        codigo += temp + " = 1;\n";
        codigo += Lf1 + ":\n";
        codigo += Lf2 + ":\n";
        let temp2 = ent3D.getT();
        codigo += temp2 + " = " + temp + ";\n";
        ent3D.suprTempNoUtil(temp);
        ent3D.addTempNoUtil(temp2);
        codigo += "#END \"Exp Lógica\"\n"
        return codigo;
    }
    traducirOr(ent3D, ent) {
        let codigo = "#BEGIN \"Expresión Lógica\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let tOp1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp1);

        let Lv = ent3D.getL();
        let Lf1 = ent3D.getL();
        let temp = ent3D.getT();
        codigo += temp + " = 0;\n";
        ent3D.addTempNoUtil(temp);
        codigo += "if (" + tOp1 + " == 1) goto " + Lv + ";\n";
        codigo += "goto " + Lf1 + ";\n";
        ent3D.suprTempNoUtil(tOp1);
        codigo += Lf1 + ":\n";

        codigo += this.op2.traducir(ent3D, ent);
        let tOp2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp2);
        let Lv2 = ent3D.getL();
        let Lf2 = ent3D.getL();
        codigo += "if (" + tOp2 + " == 1) goto " + Lv2 + ";\n";
        codigo += "goto " + Lf2 + ";\n";
        ent3D.suprTempNoUtil(tOp2);
        codigo += Lv + ":\n";
        codigo += Lv2 + ":\n";
        codigo += temp + " = 1;\n";

        codigo += Lf2 + ":\n";
        let temp2 = ent3D.getT();
        codigo += temp2 + " = " + temp + ";\n";
        ent3D.suprTempNoUtil(temp);
        ent3D.addTempNoUtil(temp2);
        codigo += "#END \"Exp Lógica\"\n"
        return codigo;
    }
    traducirXor(ent3D, ent) {
        let codigo = "#BEGIN \"Expresión Lógica\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let tOp1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp1);
        codigo += this.op2.traducir(ent3D, ent);
        let tOp2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp2);

        let temp = ent3D.getT();
        let etq1 = ent3D.getL();
        let etq2 = ent3D.getL();
        let etq3 = ent3D.getL();
        let etq4 = ent3D.getL();
        let etq5 = ent3D.getL();
        let etq6 = ent3D.getL();
        codigo += temp + " = 0;\n";

        codigo += "if (" + tOp1 + " == 1) goto " + etq1 + ";\n";
        codigo += "goto " + etq2 + ";\n";
        codigo += etq1 + ":\n";
        codigo += "if (" + tOp2 + " == 0) goto " + etq3 + ";\n";
        codigo += "goto " + etq4 + ";\n";
        codigo += etq2 + ":\n";
        codigo += "if (" + tOp2 + " == 1) goto " + etq5 + ";\n";
        codigo += "goto " + etq6 + ";\n";
        codigo += etq3 + ":\n";
        codigo += etq5 + ":\n";
        codigo += temp + " = 1;\n";
        codigo += etq4 + ":\n";
        codigo += etq6 + ":\n";

        ent3D.addTempNoUtil(temp);
        ent3D.suprTempNoUtil(tOp1);
        ent3D.suprTempNoUtil(tOp2);
        codigo += "#END \"Exp Lógica\"\n"
        return codigo;
    }
    traducirNot(ent3D, ent) {
        let codigo = "#BEGIN \"Expresión Lógica\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let temp = ent3D.getTempActual();
        ent3D.addTempNoUtil(temp);
        let temp2 = ent3D.getT();
        let Lv = ent3D.getL();
        let Lf = ent3D.getL();

        codigo += "if (" + temp + " == 1) goto " + Lv + ";\n";
        codigo += temp2 + " = 1;\n";
        codigo += "goto " + Lf + ";\n";
        codigo += Lv + ":\n";
        codigo += temp2 + " = 0;\n";
        codigo += Lf + ":\n";
        ent3D.suprTempNoUtil(temp);
        ent3D.addTempNoUtil(temp2);
        codigo += "#END \"Exp Lógica\"\n"
        return codigo;
    }

}