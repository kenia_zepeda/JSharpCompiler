class Aritmetica {

    constructor(op1, op2, op, fila, columna) {
        this.op1 = op1;
        this.op2 = op2;
        this.op = op;
        this.fila = fila;
        this.columna = columna;
    }

    analizar(ent3D, ent) {
        let tipo;

        if (this.op1 != null && this.op2 != null) {
            switch (this.op) {
                case "suma":
                    return this.getTipoSuma(ent3D, ent);
                case "resta":
                    return this.getTipoResta(ent3D, ent);
                case "multiplicacion":
                    return this.getTipoMult(ent3D, ent);
                case "division":
                    return this.getTipoDiv(ent3D, ent);
                case "potencia":
                    return this.getTipoPotenciaMod(ent3D, ent);
                case "modulo":
                    return this.getTipoPotenciaMod(ent3D, ent);
                case "unario":
                    return this.getTipoUnario(ent3D, ent);
                default:
                    let error = new ErrorAst("Semántico", "Tipo desconocido en operación aritmética", this.fila, this.columna);
                    Singleton.addError(error);
                    return error;
                }

        }

        return [tipo];
    }


    traducir(ent3D, ent) {
        if (this.op1 != null && this.op2 != null) {
            switch (this.op) {
                case "suma":
                    return this.traducirSuma(ent3D, ent);
                case "resta":
                    return this.traducirOperacion(ent3D, ent, "-");
                case "multiplicacion":
                    return this.traducirOperacion(ent3D, ent, "*");
                case "division":
                    return this.traducirOperacion(ent3D, ent, "/");
                case "unario":
                    return this.traducirUnario(ent3D, ent);
                case "potencia":
                    return this.traducirPotencia(ent3D, ent);
                case "modulo":
                    return this.traducirOperacion(ent3D, ent, "%");
                default:
                    let error = new ErrorAst("Semántico", "Tipo desconocido en operación aritmética", this.fila, this.columna);
                    Singleton.addError(error);
                    return error;
            }
        }
    }

    getTipoSuma(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];

        if (op1Tipo == "string" || op2Tipo == "string") {
            return ["string"];
        } else if (op1Tipo == "boolean" || op2Tipo == "boolean") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, booleano no se puede sumar", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "double" || op2Tipo == "double") {
            return ["double"];
        } else if (op1Tipo == "int" || op2Tipo == "int") {
            return ["int"];
        } else if (op1Tipo == "char" && op2Tipo == "char") {
            return ["char"];
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar suma", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }

    getTipoResta(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];
        if (op1Tipo == "string" || op2Tipo == "string") {
            let erkor = new ErrorAst("Semántico", "Tipo de dato incorrecto, string no se puede restar ", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "boolean" || op2Tipo == "boolean") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, booleano no se puede restar", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "double" || op2Tipo == "double") {
            return ["double"];
        } else if (op1Tipo == "int" || op2Tipo == "int") {
            return ["int"];
        } else if (op1Tipo == "char" && op2Tipo == "char") {
            return ["int"];
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar resta", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }

    getTipoMult(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];
        if (op1Tipo == "string" || op2Tipo == "string") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, string no se puede multiplicar ", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "boolean" || op2Tipo == "boolean") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, booleano no se puede multiplicar", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "double" || op2Tipo == "double") {
            return ["double"];
        } else if (op1Tipo == "int" || op2Tipo == "int") {
            return ["int"];
        } else if (op1Tipo == "char" && op2Tipo == "char") {
            return ["int"];
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar multiplicación", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }
    getTipoDiv(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];
        if (op1Tipo == "string" || op2Tipo == "string") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, string no se puede dividir ", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "boolean" || op2Tipo == "boolean") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, booleano no se puede dividir", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "double" || op2Tipo == "double") {
            return ["double"];
        } else if (op1Tipo == "int" || op2Tipo == "int") {
            return ["double"];
        } else if (op1Tipo == "char" && op2Tipo == "char") {
            return ["double"];
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar división", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }


    getTipoUnario(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        if (op1Tipo == "string") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, string no se puede negar", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "boolean") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, booleano no se puede negar", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        } else if (op1Tipo == "double") {
            return ["double"];
        } else if (op1Tipo == "int") {
            return ["int"];
        } else if (op1Tipo == "char") {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, char no se puede negar", this.fila, this.columna)
        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar negación", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }

    getTipoPotenciaMod(ent3D, ent) {
        let tipo1 = this.op1.analizar(ent3D, ent);
        let tipo2 = this.op2.analizar(ent3D, ent);
        let op1Tipo = tipo1[0];
        let op2Tipo = tipo2[0];
        if (op1Tipo == "int" && op2Tipo == "int") {
            return ["int"];

        } else {
            let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no se puede realizar operación", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
    }


    traducirSuma(ent3D, ent) {
        let codigo = "#BEGIN \"Exp Aritmética\", fila: " + this.fila + " col: " + this.columna + "\n";

        let op1tipo = this.op1.analizar(ent3D, ent);
        let tipo1 = op1tipo[0];
        codigo += this.op1.traducir(ent3D, ent);
        let tOp1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp1);

        let op2tipo = this.op2.analizar(ent3D, ent);
        let tipo2 = op2tipo[0];
        codigo += this.op2.traducir(ent3D, ent);
        let tOp2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp2);

        if (tipo1 == "string" && tipo1 == "string") {


        } else if (tipo1 == "string") {

            switch (this.tipo2) {

                case "char":

                    break;
                case "int":

                    break;
                case "double":

                    break;
                case "boolean":

                    break;
                default:
                    break;
            }


        } else if (tipo2 == "string") {

            switch (this.tipo) {

                case "char":

                    break;
                case "int":

                    break;
                case "double":

                    break;
                case "boolean":

                    break;
                default:
                    break;
            }

        } else {
            let temp = ent3D.getT();
            codigo += temp + " = " + tOp1 + " + " + tOp2 + ";\n"
            ent3D.addTempNoUtil(temp);
        }
        codigo += "#END \"Exp Aritmética\"\n"
        return codigo;
    }

    traducirOperacion(ent3D, ent, operador) {
        let codigo = "#BEGIN \"Exp Aritmética\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let tOp1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp1);

        codigo += this.op2.traducir(ent3D, ent);
        let tOp2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(tOp2);

        let temp = ent3D.getT();
        codigo += temp + " = " + tOp1 + operador + tOp2 + ";\n"
        ent3D.addTempNoUtil(temp);
        ent3D.suprTempNoUtil(tOp1);
        ent3D.suprTempNoUtil(tOp2);
        codigo += "#END \"Exp Aritmética\"\n"
        return codigo;
    }


    traducirPotencia(ent3D, ent) {

        let codigo = "#BEGIN \"Exp Aritmética\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let t1 = ent3D.getTempActual();
        ent3D.addTempNoUtil(t1);
        codigo += this.op2.traducir(ent3D, ent);
        let t2 = ent3D.getTempActual();
        ent3D.addTempNoUtil(t2);

        let Lv = ent3D.getL();
        let Lf = ent3D.getL();
        let etqC = ent3D.getL();

        let temp = ent3D.getT();

        codigo += temp + " = 1;\n";
        codigo += etqC + ":\n";
        codigo += "if ( " + t2 + " > 0 ) goto " + Lv + ";\n";
        codigo += "goto " + Lf + ";\n";
        codigo += Lv + ":\n";
        codigo += temp + " = " + temp + " * " + t1 + ";\n";
        codigo += t2 + " = " + t2 + " - 1;\n";
        codigo += "goto " + etqC + ";\n";
        codigo += Lf + ":\n";

        ent3D.suprTempNoUtil(t1);
        ent3D.suprTempNoUtil(t2);
        ent3D.addTempNoUtil(temp);
        codigo += "#END \"Exp Aritmética\"\n"
        return codigo;
    }

    traducirUnario(ent3D, ent) {

        let codigo = "#BEGIN \"Exp Aritmética\", fila: " + this.fila + " col: " + this.columna + "\n";

        codigo += this.op1.traducir(ent3D, ent);
        let temp = ent3D.getTempActual();
        ent3D.addTempNoUtil(temp);

        let temp2 = ent3D.getT();
        codigo += temp2 + " = " + temp + " * -1;\n";
        ent3D.suprTempNoUtil(temp);
        ent3D.addTempNoUtil(temp2);
        codigo += "#END \"Exp Aritmética\"\n"
        return codigo;
    }


}

