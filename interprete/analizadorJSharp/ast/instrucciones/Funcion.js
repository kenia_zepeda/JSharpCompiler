class Funcion{

    constructor(tipo, id, parametros, sentencias, fila, columna) {
        this.tipo = tipo;
        this.id = id.toLocaleLowerCase();
        this.parametros = parametros;
        this.sentencias = sentencias;
        this.fila = fila;
        this.columna = columna;
        this.id2 = "";
    }
    analizar(ent3D, ent) {
        let nombreFun = this.id;
        let nombres_par = [];
        for (let i = 0; i < this.parametros.length; i++) {
            if (this.parametros[i][0][0] == "array") {
                nombreFun += "_arreglo_" + this.parametros[i][0][1];
            } else {
                nombreFun += "_" + this.parametros[i][0];
            }

            let nom = this.parametros[i][1].toLocaleLowerCase();
            for (let j = 0; j < nombres_par.length; j++) {
                if (nom == nombres_par[j].toLocaleLowerCase()) {
                    let error = new ErrorAst("Semántico", "Parámentros duplicados", this.fila, this.columna);
                    Singleton.addError(error);
                    return error;
                }
            }
        }
        /// verificar que el tipo exista si es struc
        if (ent3D.existeFun(nombreFun)) {
            let err = new ErrorAst("Semántico", "Función \"" + this.id +"\" ya ha sido declarada", this.fila, this.columna);
            Singleton.addError(error);
            return error;
        }
        this.id2 = nombreFun;
        let fun = new FuncionSim(nombreFun, this.id, this.tipo, this.parametros, this.sentencias, this.parametros.length + 1);
        ent3D.addFuncion(fun);
        ent3D.simbolos.push(fun);
    }
    traducir(ent3D, ent) {
        let codigo = "#----FUNCION: " + this.id + ", fila: " + this.fila + " col: " + this.columna + "----\n";


        codigo += "proc " + this.id2 + " begin\n";

        let entornoLocal = new Entorno(ent);
        ent3D.stack = 1;
        let etqS = ent3D.getL();
        ent3D.tipoFuncion = this.tipo;
        ent3D.finFuncion = etqS;
        ent3D.entorno = "local";
        ent3D.ambito = this.id2;


        for (let i = 0; i < this.parametros.length; i++) {
            let sim = new Simbolo(this.parametros[i][0], "Parametro", this.parametros[i][1].toLocaleLowerCase(), null, "local", ent3D.ambito, ent3D.getStack(), false, false, null);
            entornoLocal.agregar(this.parametros[i][1].toLocaleLowerCase(),sim);
            ent3D.simbolos.push(sim);
        }
        for (let i = 0; i < this.sentencias.length; i++) {
            let sentencia = this.sentencias[i];
            if (!(sentencia instanceof Estructura)) {
                if (sentencia instanceof Declaracion2) {
                    if (sentencia.tipo != "global") {
                        let val = sentencia.analizar(ent3D,entornoLocal);
                    }
                } else {
                    let val = sentencia.analizar(ent3D,entornoLocal);
                }
            }
        }
        if (Singleton.errores.length > 0) {
            ent3D.tipoFuncion = ["void"];
            ent3D.finFuncion = "";
            return Singleton.errores[0];
        }
        for (let i = 0; i < this.sentencias.length; i++) {
            codigo += this.sentencias[i].traducir(ent3D, entornoLocal);
        }
        ent3D.entorno = "global";
        ent3D.ambito = "global";
        ent3D.tipoFuncion = ["void"];
        ent3D.finFuncion = "";
       // ent3D.locales = new Entorno(null);
        ent3D.stack = 0;
        codigo += etqS + ":\n";
        codigo += "end\n";
        codigo += "#---------END FUNCION-------------\n";
        return codigo;
    }
}