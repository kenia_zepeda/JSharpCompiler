class Declaracion2 {

    constructor(tipo, id, expr, fila, columna) {
        this.tipo = tipo;
        this.expr = expr;
        this.id = id;
        this.fila = fila;
        this.columna = columna;
    }

    analizar(ent3D, ent) {

        if (this.expr != null) {
            let tipoExp = this.expr.analizar(ent3D, ent);

            if (this.tipo == "global") {
                if (ent.existeGlobal(this.id)) {
                    let error = new ErrorAst("Semántico", "Variable \"" + this.id +"\" ya declarada en el entorno global",this.fila, this.columna)
                    Singleton.addError(error);
                    return error;
                } else {
                    //constructor(tipo, rol, this.id, expr, entorno, ambito, puntero, flagArray, flagConst, dims) ;
                    let sim = new Simbolo(tipoExp, "variable", this.id.toLocaleLowerCase(), this.expr, "global", ent3D.ambito, ent3D.getHeap(), false, false, null);
                    ent.agregarGlobal(this.id, sim);
                    ent3D.agregarGlobal(sim);
                    ent3D.simbolos.push(sim);
                }
            } else {
                let constFlag = false;
                if (this.tipo == "const") {
                    constFlag = true;
                }
                if (ent3D.entorno == "global") {
                    if (ent.existeGlobal(this.id)) {
                        let error = new ErrorAst("Semántico", "Variable \"" + this.id +"\" ya declarada en el entorno global",this.fila, this.columna)
                        Singleton.addError(error);
                        return error;
                    } else {
                        //constructor(tipo, rol, this.id, expr, entorno, ambito, puntero, flagArray, flagConst, dims) ;
                        let sim = new Simbolo(tipoExp, "variable", this.id.toLocaleLowerCase(), this.expr, "global", ent3D.ambito, ent3D.getHeap(), false, constFlag, null);
                        ent.agregarGlobal(this.id, sim);
                        ent3D.agregarGlobal(sim);
                        ent3D.simbolos.push(sim);
                    }
                } else {
                    if (ent.existeActual(this.id)) {
                        let error = new ErrorAst("Semántico", "Variable \"" + this.id +"\" ya declarada en el entorno local",this.fila, this.columna)
                        Singleton.addError(error);
                        return error;
                    } else {
                        //constructor(tipo, rol, this.id, expr, entorno, ambito, puntero, flagArray, flagConst, dims) ;
                        let sim = new Simbolo(tipoExp, "variable", this.this.id.toLocaleLowerCase(), this.expr, "local", ent3D.ambito, ent3D.getHeap(), false, constFlag, null);
                        ent.agregar(this.id, sim);
                        ent3D.simbolos.push(sim);
                    }
                }
            }
            return tipoExp;

        } else {
            let error = new ErrorAst("Semántico", "Declaración de la variable \"" + this.id +"\" debe tener asignado un valor", this.fila, this.columna);
            Singleton.addError(error);
            return error;
        }

    }

    traducir(ent3D, ent) {
        let codigo = "#BEGIN Declaracion de "+ this.tipo+", fila: " + this.fila + " columna: " + this.columna + "\n";
        let sim = ent.getSimbolo(this.id.toLocaleLowerCase());
        codigo += this.expr.traducir(ent3D, ent);
        let tV = ent3D.getTempActual();
        ent3D.suprTempNoUtil(tV);
        if (sim.entorno == "local") {
            let temp = ent3D.getT();
            codigo += temp + " = p + " + sim.puntero + ";\n";
            codigo += "Stack[" + temp + "] = " + tV + ";\n";
        } else {
            let temp = ent3D.getT();
            codigo += temp + " = " + sim.puntero + ";\n";
            codigo += "Heap[" + temp + "] = " + tV + ";\n";
        }
        codigo += "#END Declaracion\n"
        return codigo;
    }

}