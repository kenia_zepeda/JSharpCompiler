class Imprimir{

    constructor(expr, fila, columna) {
        this.expr = expr;
        this.fila = fila;
        this.columna = columna;
    }
    analizar(ent3D, ent) {
        let val = this.expr.analizar(ent3D, ent);
        if (val instanceof ErrorAst) {
            return val;
        }
        if (val == "void") {
            let err = new ErrorAst("Semántico", "No se puede imprimir una funcion void", this.expr.fila, this.expr.columna);
            Singleton.addError(error);
            return error;
        }
    }
    traducir(ent3D, ent) {
        let codigo = "#BEGIN \"Print\", fila: " + this.fila + " col: " + this.columna + "\n";
        let tipo = this.expr.analizar(ent3D, ent);
        codigo += this.expr.traducir(ent3D, ent);
        let temp = ent3D.getTempActual();
        console.log("imprimir " + tipo);
        if (tipo[0] == "int") {
            codigo += "print(\"%i\", " + temp + ");\n";
        } else if (tipo[0] == "double") {
            codigo += "print(\"%d\", " + temp + " );\n";
        } else if (tipo[0] == "boolean") {
            let Lv = ent3D.getL();
            let Lf = ent3D.getL();
            codigo += "if (" + temp + " == 1) goto " + Lv + ";\n";
            codigo += "print(\"%c\", 102);\n";
            codigo += "print(\"%c\", 97);\n";
            codigo += "print(\"%c\", 108);\n";
            codigo += "print(\"%c\", 115);\n";
            codigo += "print(\"%c\", 101);\n";
            codigo += "goto " + Lf + ";\n"
            codigo += Lv + ":\n";
            codigo += "print(\"%c\", 116);\n";
            codigo += "print(\"%c\", 114);\n";
            codigo += "print(\"%c\", 117);\n";
            codigo += "print(\"%c\", 101);\n";
            codigo += Lf + ":\n"
        } else if (tipo[0] == "null") {
            codigo += "print(\"%c\", 110);\n";
            codigo += "print(\"%c\", 117);\n";
            codigo += "print(\"%c\", 108);\n";
            codigo += "print(\"%c\", 108);\n";
        }
        else if (tipo[0] == "char") {
            codigo += "print(\"%c\", " + temp + ");\n";
        } else if (tipo = "string") {
            let tempLetra = ent3D.getT();
            let Lv = ent3D.getL();
            let Lf = ent3D.getL();
            let tempTam = ent3D.getT();
            let Lv2 = ent3D.getL();
            codigo += "if (" + temp + " == -1) goto " + Lv2 + ";\n";
            codigo += tempTam + " = Heap[" + temp + "];\n"
            codigo += temp + " = " + temp + " + 1;\n"
            codigo += Lf + ":\n"
            codigo += "if (" + tempTam + " <= 0) goto " + Lv + ";\n";
            codigo += tempLetra + " = Heap[" + temp + "];\n";
            codigo += "print(\"%c\", " + tempLetra + ");\n";
            codigo += temp + " = " + temp + " + 1;\n"
            codigo += tempTam + " = " + tempTam + " - 1;\n";
            codigo += "goto " + Lf + ";\n";
            codigo += Lv + ":\n";
            codigo += Lv2 + ":\n";
        }
        ent3D.suprTempNoUtil(temp);
        codigo += "print(\"%c\", 13);\n";
        codigo += "#END \"Print\"\n"
        return codigo;
    }

}

