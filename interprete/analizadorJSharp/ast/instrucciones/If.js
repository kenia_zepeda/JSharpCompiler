class If{
    constructor(condicion, sentencias, sentenciasElse, fila, columna) {
        this.condicion = condicion;
        this.sentencias = sentencias;
        this.sentenciasElse = sentenciasElse;
        this.fila = fila;
        this.columna = columna;
        this.local = null;
        this.localElse = null;
    }
    analizar(ent3D, ent) {
        let con = this.condicion.analizar(ent3D, ent);
        if (con instanceof ErrorAst) {
            return con;
        }

        if (con[0] != 'boolean') {
            let err = new ErrorAst("Semántico", "Tipo de dato incorrecto, se espera un booleano", this.fila, this.columna);
            Singleton.addError(error);
            return error;
        }
        let local = new Entorno(ent3D.locales);
        ent3D.locales = local;
        for (let x = 0; x < this.sentencias.length; x++) {
            let sentencia = this.sentencias[x];
            if (!(sentencia instanceof DefinirEstructura)) {
                if (sentencia instanceof DeclaracionSinTipoAlto) {
                    if (sentencia.tipo != "global") {
                        let res = sentencia.analizar(ent3D, ent);
                        if (res instanceof ErrorAst) {
                            return res;
                        }
                    }
                } else {
                    let res = sentencia.analizar(ent3D, ent);
                    if (res instanceof ErrorAst) {
                        return res;
                    }
                }
            }
        }
        this.local = local;
        ent3D.locales = local.anterior;

        let localElse = new Entorno(ent3D.locales);
        ent3D.locales = localElse;
        if (this.sentenciasElse != null) {
            for (let x = 0; x < this.sentenciasElse.length; x++) {
                let sentencia = this.sentenciasElse[x];
                if (!(sentencia instanceof DefinirEstructura)) {
                    if (sentencia instanceof DeclaracionSinTipoAlto) {
                        if (sentencia.tipo != "global") {
                            let res = sentencia.analizar(ent3D, ent);
                            if (res instanceof ErrorAst) {
                                return res;
                            }
                        }
                    } else {
                        let res = sentencia.analizar(ent3D, ent);
                        if (res instanceof ErrorAst) {
                            return res;
                        }
                    }
                }
            }
        }

        this.localElse = localElse;
        ent3D.locales = localElse.anterior;

    }
    traducir(ent3D, ent) {
        let codigo = "#BEGIN IF fila: " + this.fila + " columna: " + this.columna + "\n";
        codigo += this.condicion.traducir(ent3D, ent);
        let temp = ent3D.getTempActual();
        let Lv = ent3D.getL();
        let Lf = ent3D.getL();
        codigo += "if (" + temp + " == 1) goto " + Lv + ";\n"
        ent3D.suprTempNoUtil(temp);
        if (this.sentenciasElse != null) {
            codigo += "# Traducion Else:\n";
            ent3D.locales = this.localElse;
            this.sentenciasElse.map(m => {
                let cod = m.traducir(ent3D, ent);
                if (cod instanceof ErrorAst) {
                    return cod;
                }
                codigo += cod;
            });
            ent3D.locales = this.localElse.anterior;
            codigo += "# Fin Traducion Else\n";
        }
        codigo += "goto " + Lf + ";\n";
        codigo += Lv + ":\n";
        ent3D.locales = this.local;
        this.sentencias.map(m => {
            let cod = m.traducir(ent3D, ent);
            if (cod instanceof ErrorAst) {
                return cod;
            }
            codigo += cod;
        });
        ent3D.locales = this.local.anterior;
        codigo += Lf + ":\n";
        codigo += "#END IF\n"
        return codigo;
    }
    
}