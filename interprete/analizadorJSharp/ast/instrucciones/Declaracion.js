class Declaracion {

    constructor(tipo, listaid, expr, fila, columna) {
        this.tipo = tipo;
        this.expr = expr;
        this.listaid = listaid;
        this.fila = fila;
        this.columna = columna;
    }

    analizar(ent3D, ent) {
        let tipoExp = null;
        if (this.expr != null) {
            tipoExp = this.expr.analizar(ent3D, ent);
            if (tipoExp[0] != this.tipo[0]) {
                let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no coindice el tipo de dato de la variable  \"" + this.id +"\" con el valor asignado", this.fila, this.columna);
                Singleton.addError(error);
                return error;
            }
        }

        if (ent3D.entorno == "global") {
                for (var i in this.listaid) {
                    var id = this.listaid[i];
                    if (ent.existe(id)) {
                        let error = new ErrorAst("Semántico", "Variable \"" + this.id +"\" ya declarada en el entorno global",this.fila, this.columna)
                        Singleton.addError(error);
                        return error;
                    } else {
                        //constructor(tipo, rol, id, expr, entorno, ambito, puntero, flagArray, flagConst, dims) ;
                        let sim = new Simbolo(this.tipo, "variable", id.toLocaleLowerCase(), this.expr, "global", ent3D.ambito, ent3D.getHeap(), false, false, null);
                        ent.agregarGlobal(id, sim);
                        ent3D.agregarGlobal(sim);
                        ent3D.simbolos.push(sim);
                    }
                }
        } else {
            for (var i in this.listaid) {
                var id = this.listaid[i];
                if (ent.existeActual(id)) {
                    let error = new ErrorAst("Semántico", "Variable \"" + this.id +"\" ya declarada en el entorno actual", this.fila, this.columna)
                    Singleton.addError(error);
                    return error;
                } else {
                    //constructor(tipo, rol, id, expr, entorno, ambito, puntero, flagArray, flagConst, dims) ;
                    let sim = new Simbolo(this.tipo, "variable", this.id.toLocaleLowerCase(), this.expr, "local", ent3D.ambito, ent3D.getHeap(), false, false, null);
                    ent.agregar(id, sim);
                    ent3D.simbolos.push(sim);
                }
            }
        }
        return tipoExp;

    }

    traducir(ent3D, ent) {
        let codigo = "#BEGIN declaración, fila: " + this.fila + " columna: " + this.columna + "\n";
        if (this.expr == null) {
            for (let i = 0; i < this.listaid.length; i++) {
                let sim = ent.getSimbolo(this.listaid[i].toLocaleLowerCase());
                if (sim.entorno == "local") {
                    let temp = ent3D.getT();
                    codigo += temp + " = p + " + sim.puntero + ";\n";
                    if (sim.tipo[0] == "integer" || sim.tipo[0] == "boolean" || sim.tipo[0] == "char") {
                        codigo += "Stack[" + temp + "] = 0;\n";
                    } else if (sim.tipo[0] == "double") {
                        codigo += "Stack[" + temp + "] = 0.0;\n";
                    } else {
                        codigo += "Stack[" + temp + "] = -1;\n";
                    }
                } else {
                    let temp = ent3D.getT();
                    codigo += temp + " = " + sim.puntero + ";\n";
                    if (sim.tipo[0] == "integer" || sim.tipo[0] == "boolean" || sim.tipo[0] == "char") {
                        codigo += "Heap[" + temp + "] = 0;\n";
                    } else if (sim.tipo[0] == "double") {
                        codigo += "Heap[" + temp + "] = 0.0;\n";
                    } else {
                        codigo += "Heap[" + temp + "] = -1;\n";
                    }
                }
            }
        }
        else {
            for (let i = 0; i < this.listaid.length; i++) {
                let sim = ent.getSimbolo(this.listaid[i].toLocaleLowerCase());
                codigo += this.expr.traducir(ent3D, ent);
                let tV = ent3D.getTempActual();
                ent3D.suprTempNoUtil(tV);
                if (sim.entorno == "local") {
                    let temp = ent3D.getT();
                    codigo += temp + " = p + " + sim.puntero + ";\n";
                    codigo += "Stack[" + temp + "] = " + tV + ";\n";
                } else {
                    let temp = ent3D.getT();
                    codigo += temp + " = " + sim.puntero + ";\n";
                    codigo += "Heap[" + temp + "] = " + tV + ";\n";
                }
            }
        }
        codigo += "#END Declaracion\n"
        return codigo;
    }
}

