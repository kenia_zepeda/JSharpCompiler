class Asignacion {
    constructor(id, expr, fila, columna) {
        this.id = id;
        this.expr = expr;
        this.fila = fila;
        this.columna = columna;
    }
    analizar(ent3D, ent) {
        if (!ent.existe(this.id)) {
            let error = new ErrorAst("Semántico", "Variable \"" + this.id + "\" no esta declarada", this.fila, this.columna)
            Singleton.addError(error);
            return error;
        }
        let tipoExpr = this.expr.analizar(ent);
        if (tipoExpr instanceof ErrorAst) {
            return err;
        }
        let sim = ent.getSimbolo(this.id);
        if (sim.constante) {
            let erro = new ErrorAst("Semantico", "Variable \"" + this.id + "\" no se puede modificar porque es una constante", this.fila, this.columna);
            Singleton.addError(error);
            return error;
        }
        if (sim.tipo[0] != tipoExpr[0]) {
            if (!((sim.tipo[0] == "int" && tipoExpr[0] == "char")
                || (sim.tipo[0] == "double" && tipoExpr[0] == "char")
                || (sim.tipo[0] == "double" && tipoExpr[0] == "int")
                || (sim.tipo[0] == "string" && tipoExpr[0] == "null")
                || (sim.tipo[0] == "array" && tipoExpr[0] == "null")
                || (ent3D.existeEstr(sim.tipo[0]) && tipoExpr[0] == "null")
            )) {
                let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no coindice el tipo de dato de la variable  \"" + this.id + "\" con el valor asignado", this.fila, this.columna);
                Singleton.addError(error);
                return error;
            }
        } else if (sim.tipo[0] == "array" && tipoExpr[0] == "array") {
            if (sim.tipo[1] != tipoExpr[1]) {
                let error = new ErrorAst("Semántico", "Tipo de dato incorrecto, no coindice el tipo de dato de la variable  \"" + this.id + "\" con el valor asignado", this.fila, this.columna);
                Singleton.addError(error);
                return error;
            }
        }
    }
    traducir(ent3D, ent) {
        let codigo = "# Inicio Asignacion fila " + this.fila + " columna " + this.columna + "\n";
        let sim = ent.getSimbolo(this.id);
        codigo += this.expr.traducir(ent3D, ent);
        let tV = ent3D.getTempActual();
        ent3D.suprTempNoUtil(tV);
        if (sim.entorno == "local") {
            let temp = ent3D.getT();
            codigo += temp + " = p + " + sim.puntero + ";\n";
            codigo += "Stack[" + temp + "] = " + tV + ";\n";
        } else {
            let temp = ent3D.getT();
            codigo += temp + " = " + sim.puntero + ";\n";
            codigo += "Heap[" + temp + "] = " + tV + ";\n";
        }
        codigo += "# Fin Asignacion\n";
        return codigo;
    }

}